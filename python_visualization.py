# -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import numpy as np
import matplotlib.pyplot as plt
cycle_colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]
tkw = dict(size=4, width=1.5)
###############################################################################
# Header
###############################################################################
__author__ = ['Benjamin Loeffel']
__maintainer__ = ['Benjamin Loeffel']
__email__ = ['benjamin.loeffel@fischerspindle.ch']
###############################################################################
# Constants
###############################################################################
n=np.array([6,5,4,3],)
borders_right=np.array([0.75,0.8,0.84,0.89])
border_right_interpolation=np.polyfit(n,borders_right,1)
###############################################################################
# Methods
###############################################################################
def set_figure_defaults():
    import matplotlib as mpl
    mpl.rcParams["figure.figsize"] = (18,9)
    mpl.rcParams["figure.subplot.left"]=0.06
    mpl.rcParams["figure.subplot.bottom"]=0.07
    mpl.rcParams["figure.subplot.right"]=0.94
    mpl.rcParams["figure.titlesize"]=22
    mpl.rcParams["figure.titleweight"]="normal"
    mpl.rcParams["axes.labelsize"]=14
    
def maximize_figures():
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    ###############################################################################
def plot_data_list(title,x_data_list,y_data_list,x_label,y_label_list,legend_loc=0,axis_limits=None):
    """
    Plots the specified variables over time
    
    Parameters
    ----------
    title : scalar, string
        Title of the plot to generate
    x_data_list : list, np.array
        list of arrays containing the x data to plot
    y_data_list : list, np.array
        list of arrays containing the y data to plot
    x_label : scalar, string
        x_label
    y_label_list : list, string
        list of label strings used for axes
    legend_loc : scalar, int, optional
        Location of legend accoring to https://matplotlib.org/api/legend_api.html, default = 0 (best)
    axis_limits : list of lists, optional
        list of lists containig the both upper and lower y axis limits for the corresponding channel, default = None so autoscaling happens
    """
    #######################################################################
    # Settings
    
    right_position=1.075
    right_position_increment = 0.07
    adjust=np.polyval(border_right_interpolation,len(y_data_list))
    
    #######################################################################
    # Preamble
    fig, host = plt.subplots()
    fig.canvas.set_window_title(title)
    plt.subplots_adjust(right=adjust)
    
    axes_list=[]
    axes_list.append(host)
    axes_list.append(host.twinx())
    
    #######################################################################
    # Generating axes     
    
    for index_channel in range(len(x_data_list)-2):
        par=host.twinx()
        par.spines["right"].set_position(("axes", right_position))
        _make_patch_spines_invisible(par)
        par.spines["right"].set_visible(True)

        axes_list.append(par)

        right_position+=right_position_increment
        
    #######################################################################
    # Generating plots

    plots=[]
    
    for index_channel in range(len(x_data_list)):
        x_data=x_data_list[index_channel]
        y_data=y_data_list[index_channel]
        label=y_label_list[index_channel]
        
        axis=axes_list[index_channel]
        color=cycle_colors[index_channel]
                    
        plot,=axis.plot(x_data,y_data,color=color,label=label)
        plots.append(plot)
        
    #######################################################################
    # Layouting
        
    for index_channel in range(len(x_data_list)):
        label=y_label_list[index_channel]
        axis=axes_list[index_channel]
        plot=plots[index_channel]

        if axis_limits!=None:
            axis_limit=axis_limits[index_channel]
            axis.set_ylim(axis_limit)

        if index_channel==0:
            axis.set_xlabel(x_label)
            axis.set_ylabel(label)
            axis.grid(axis="both")
            axis.yaxis.label.set_color(plot.get_color())
            
            axis.tick_params(axis='x', **tkw)
            axis.tick_params(axis='y', colors=plot.get_color(), **tkw)

        else:
            axis.set_ylabel(label)
            axis.yaxis.label.set_color(plot.get_color())
            
            axis.tick_params(axis='y', colors=plot.get_color(), **tkw)
    
    if legend_loc != None:
        host.legend(plots, [l.get_label() for l in plots],loc=legend_loc)
    
    plt.suptitle(title,y=0.96)
    return fig
           
def _make_patch_spines_invisible(ax):
    """
    Helper method for neat plot
    """
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
        
def align_yaxis_np(ax1, ax2):
    """Align zeros of the two axes, zooming them out by same ratio"""
    axes = np.array([ax1, ax2])
    extrema = np.array([ax.get_ylim() for ax in axes])
    tops = extrema[:,1] / (extrema[:,1] - extrema[:,0])
    # Ensure that plots (intervals) are ordered bottom to top:
    if tops[0] > tops[1]:
        axes, extrema, tops = [a[::-1] for a in (axes, extrema, tops)]

    # How much would the plot overflow if we kept current zoom levels?
    tot_span = tops[1] + 1 - tops[0]

    extrema[0,1] = extrema[0,0] + tot_span * (extrema[0,1] - extrema[0,0])
    extrema[1,0] = extrema[1,1] + tot_span * (extrema[1,0] - extrema[1,1])
    [axes[i].set_ylim(*extrema[i]) for i in range(2)]
###############################################################################       
def plot_twiny_data_list(title,left_x,left_y,right_x,right_y,x_label,left_label,right_label,legend_list,legend_loc=0,left_axis_limits=None,right_axis_limits=None):
    """
    Plots the specified variables over time
    
    Parameters
    ----------
    title : scalar, string
        Title of the plot to generate
    time_stamps : np.array
        np.array containing the timestamps
    left_x : list, np.array
        array to plot for the left y axis
    left_y : list, np.array
        array to plot for the left y axis
    right_data: list, np.array
        list of arrays to plot for the right y axis
    left_label : scalar, string
        left y axis label
    right_label : scalar, string
        right y axis label
    legend_list : list, string
        list of legend strings used for axes
    legend_loc : scalar, int, optional
        Location of legend accoring to https://matplotlib.org/api/legend_api.html, default = 0 (best)
    """
    fig, ax0 = plt.subplots()
    plt.suptitle(title,y=0.95)
    ax1=plt.twinx(ax0)
    fig.canvas.set_window_title(title)
        
    plots=[]

    plot,=ax0.plot(left_x,left_y,label=legend_list[0])
    ax0.grid(which="both", axis="both")
    plots.append(plot)

    plot,=ax1.plot(right_x,right_y,color=cycle_colors[1],label=legend_list[1])
    plots.append(plot)
        
    ax0.set_xlabel(x_label)
    ax0.set_ylabel(left_label)
    ax1.set_ylabel(right_label)
    
    ax0.yaxis.label.set_color(cycle_colors[0])
    ax0.tick_params(axis='y', colors=cycle_colors[0])
    
    ax1.yaxis.label.set_color(cycle_colors[1])
    ax1.tick_params('y', colors=cycle_colors[1])
    
    ax0.legend(plots, [l.get_label() for l in plots],loc=legend_loc)
    
    if left_axis_limits != None:
        ax0.set_ylim(left_axis_limits)
        
    if right_axis_limits != None:
        ax1.set_ylim(right_axis_limits)
        
    return fig

def align_yaxis(ax1, v1, ax2, v2):
    """
    adjust ax2 ylimit so that v2 in ax2 is aligned to v1 in ax1
    """
    _, y1 = ax1.transData.transform((0, v1))
    _, y2 = ax2.transData.transform((0, v2))
    inv = ax2.transData.inverted()
    _, dy = inv.transform((0, 0)) - inv.transform((0, y1-y2))
    miny, maxy = ax2.get_ylim()
    ax2.set_ylim(miny+dy, maxy+dy)
###############################################################################  
def plot_2D_array(data,xdata,ydata,title,xlabel,ylabel,colorbar_label):
        """
        Plots a 2D array
        
        Parameters
        ----------
        data : 2D np.array
            data to plot
        xdata : np.array
            xdata for axis scaling
        ydata : np.array
            ydata for axis scaling
        title scalar, string
            title of the plot
        xlabel : scalar, string
            xlabel of the plot
        ylabel : scalar, string
            ylabel of the plot
        colorbar_label : scalar, string
            colorbar_label of the plot
        """
        figure=plt.figure()
        plt.imshow(data, aspect="auto", interpolation="none",
                   extent=_extents(xdata) + _extents(xdata), origin="lower")
        figure.subplots_adjust(top=0.94,bottom=0.06,left=0.06,right=1.0)
        plt.title(title)
        colorbar=plt.colorbar()
        colorbar.set_label(colorbar_label)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        
def _extents(f):
    """
    Performs scaling
    """
    delta = f[1] - f[0]
    return [f[0] - delta/2, f[-1] + delta/2]    
############################################################################### 
def downsample(data,n_start=0,n_stop=-1,n_downsample=100):
    """
    performs a indexed downsampling for easier plotting
    
    Parameters
    ----------
    data: np.array
        array containing the data to downsample
    n_start : scalar, int
        starting indices of indexing
    n_stop : scalar, int
        stopping indices of indexing, for end set -1
        
    Returns
    -------
    indexes, data : tuple
        indexes of the picked elements, downsampled data
    """
    if n_stop==-1:
        n_stop=len(data)
    data=data[n_start:n_stop:n_downsample]
    return data
###############################################################################    
class FigureContainer:
    def __init__(self,figure,file_name):
        """
        FigureContainer object for easy plot export
        
        Parameters
        ----------
        figure : matplotlib.figure.Figure
            MAtplotlib figure object
        file_name : scalar, string
            Path to the filename under which the figure should be saved
        """
        self.figure=figure
        self.file_name=file_name
        
    def export_plot(self,close_figure=True):
        """
        Exports the figure
        """
        self.figure.savefig(self.file_name)
        if close_figure==True:
            plt.close(self.figure)
        
def export_plots(figure_container_list,close_figures=True):
    """
    Exports all plots contained in list
    
    Parameters
    ----------
    figure_container_list : list, python_visualization.FigureContainer
        list of FigureContainer objects
    """        
    for figure_container in figure_container_list:
        figure_container.export_plot(close_figure=close_figures)